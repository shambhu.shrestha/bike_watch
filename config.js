const credentials = require('./credentials');
const common_config = {
    credentials: credentials.default,
    url: 'https://tcc.docomo-cycle.jp/cycle/TYO/cs_web_main.php?AreaID=3',
    refreshInterval: 10*1000,
    playSound: true
};

const home_config = {
    ...common_config,
    location: '赤坂・青山/Akasaka・Aoyama',
    stations: ['C3-06', 'C3-07', 'C3-02'] //In order of priority
};

const work_config = {
    ...common_config,
    location: '新橋・虎ノ門/Shimbashi・Toranomon',
    stations: ['C1-04', 'C1-27']
};

const hanako_config = {
    ...common_config,
    credentials: credentials.hanako,
    location: '赤坂・青山/Akasaka・Aoyama',
    stations: ['C3-06', 'C3-07'], //In order of priority
    playSound: true
}


function get_config(options){
    if (options.work) return work_config;
    if (options.hanako) return hanako_config;
    return home_config;
}

module.exports = get_config;
